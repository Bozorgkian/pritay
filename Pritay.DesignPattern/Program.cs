﻿using System;

namespace Pritay.DesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            BehavioralPatterns.StrategyPattern();

            Console.ReadLine();
        }

    }

}
