﻿using Pritay.DesignPattern.Strategy;
using System;

namespace Pritay.DesignPattern
{
    public static class CreationalPatterns
    {

    }

    public static class StructuralPatterns
    {

    }

    public static class BehavioralPatterns
    {
        /// <summary>
        /// Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it.	
        /// </summary>
        public static void StrategyPattern()
        {
            Console.WriteLine("======================!");
            Console.WriteLine("Welcome Strategy Pattern!");

            PaymentStrategyContext payment1 = new PaymentStrategyContext(new SalaryPayment("Ehsan", 1000));
            PaymentStrategyContext payment2 = new PaymentStrategyContext(new BonusPayment("Amir", 100));
            PaymentStrategyContext payment3 = new PaymentStrategyContext(new GiftPayment("Ali", 10));

            payment1.Pay();
            payment2.Pay();
            payment3.Pay();

            Console.WriteLine("Enjoy Strategy Pattern!");
            Console.WriteLine("======================!");
        }

    }

}
