﻿namespace Pritay.DesignPattern.Strategy
{
    public class PaymentStrategyContext
    {
        private PaymentStrategy _strategy;

        public PaymentStrategyContext(PaymentStrategy strategy)
        {
            this._strategy = strategy;
        }

        public void Pay()
        {
            _strategy.Pay();
        }
    }

}
