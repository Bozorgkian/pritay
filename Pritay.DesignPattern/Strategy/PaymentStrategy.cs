﻿using System;

namespace Pritay.DesignPattern.Strategy
{
    public abstract class PaymentStrategy
    {
        internal PaymentStrategyParameter _parameters = new PaymentStrategyParameter();

        public PaymentStrategy(string employeeName, double amount)
        {
            _parameters = new PaymentStrategyParameter
            {
                Amount = amount,
                EmployeeName = employeeName,
                PaymentDate = DateTime.Now
            };
        }

        public abstract void Pay();
    }

}
