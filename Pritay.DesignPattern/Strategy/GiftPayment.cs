﻿using System;

namespace Pritay.DesignPattern.Strategy
{
    public class GiftPayment : PaymentStrategy
    {
        string _messgePattrern = "Gift Value {0}$ Paid at {1} to {2}";
        public GiftPayment(string employeeName, double amount) : base(employeeName, amount)
        {

        }

        public override void Pay()
        {
            var message = string.Format(_messgePattrern, _parameters.Amount, _parameters.PaymentDate, _parameters.EmployeeName);
            Console.WriteLine(message);
        }
    }

}
