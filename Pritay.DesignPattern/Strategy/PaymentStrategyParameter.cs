﻿using System;

namespace Pritay.DesignPattern.Strategy
{
    public class PaymentStrategyParameter
    {
        public DateTime PaymentDate { get; set; }

        public double Amount { get; set; }

        public string EmployeeName { get; set; }
    }

}
